# MPNST
Code/scripts for the analysis of MPNST related datasets

-- Paper: Veena Kochat\*, AT Raman\* et al., 2021 (In revision)

-- Dataset: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE141439 (currently private; will be made public soon). The GEO repository consists of all the fastq and processed files such as raw fastq (sequence files), raw counts table, list of differentially expressed genes (output from DESeq2) and bigWig/peak files (for IGV visualization).

-- Here is the pipeline for the analysis of ChIPseq dataset: https://zenodo.org/record/819971 (developed by Ming Tang)

Most updated version of the repository is also maintained here: https://github.com/aayushraman/mpnst
